using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PipLightSystem
{ 
	static PipLightSystem m_Instance;

	public static PipLightSystem instance {
		get {
			if (m_Instance == null)
				m_Instance = new PipLightSystem ();
			return m_Instance;
		}
	}

	internal HashSet<PipLight> m_Lights = new HashSet<PipLight> ();

	public void Add (PipLight o)
	{
		Remove (o);
		m_Lights.Add (o);
	}

	public void Remove (PipLight o)
	{
		m_Lights.Remove (o);
	}
}

[ExecuteInEditMode()]
public class PipLightRenderer : MonoBehaviour
{
	public static Camera[] SceneViewCameras;
	public static Shader DepthShader = null;
	public static Shader LightShader = null;

	static Dictionary<Camera, CommandBuffer> buffers = new Dictionary<Camera, CommandBuffer> ();

	public Shader lightShader;
	public Shader depthShader;
	public Mesh lightSphereMesh;

	Material PointNoShadows;
	Material PointHardShadows;
	Material PointSoftShadows;
	Material PointCookieNoShadows;
	Material PointCookieHardShadows;
	Material PointCookieSoftShadows;
  

	Material GetMaterial (PipLight light)
	{
		if (light.shadowType == LightShadows.None) {
			return light.cookie == null ? PointNoShadows : PointCookieNoShadows;
		} else if (light.shadowType == LightShadows.Hard) {
			return light.cookie == null ? PointHardShadows : PointCookieHardShadows;
		} else {
			return light.cookie == null ? PointSoftShadows : PointCookieSoftShadows;
		}
	}

	public void OnDisable ()
	{
		foreach (var pair in buffers) {
			if (pair.Key != null) {
				pair.Key.RemoveCommandBuffer (CameraEvent.BeforeImageEffectsOpaque, pair.Value);
			}
		}
		DestroyImmediate (PointNoShadows);
		DestroyImmediate (PointHardShadows);
		DestroyImmediate (PointSoftShadows);
		DestroyImmediate (PointCookieNoShadows);
		DestroyImmediate (PointCookieHardShadows);
		DestroyImmediate (PointCookieSoftShadows);
	}

	public void OnEnable ()
	{
		PipLight.CheckKeywords ();

		LightShader = lightShader;
		DepthShader = depthShader;

		PointNoShadows = new Material (lightShader);
		PointHardShadows = new Material (lightShader);
		PointSoftShadows = new Material (lightShader);
		PointCookieNoShadows = new Material (lightShader);
		PointCookieHardShadows = new Material (lightShader);
		PointCookieSoftShadows = new Material (lightShader);

		PointNoShadows.hideFlags = HideFlags.HideAndDontSave;
		PointHardShadows.hideFlags = HideFlags.HideAndDontSave;
		PointSoftShadows.hideFlags = HideFlags.HideAndDontSave;
		PointCookieNoShadows.hideFlags = HideFlags.HideAndDontSave;
		PointCookieHardShadows.hideFlags = HideFlags.HideAndDontSave;
		PointCookieSoftShadows.hideFlags = HideFlags.HideAndDontSave;

		PointNoShadows.shaderKeywords = new string[] {"POINT"};
		PointHardShadows.shaderKeywords = new string[] {"POINT", "SHADOWS_CUBE"};
		PointSoftShadows.shaderKeywords = new string[] {"POINT", "SHADOWS_CUBE", "SHADOWS_SOFT"};
		PointCookieNoShadows.shaderKeywords = new string[] {"POINT_COOKIE"};
		PointCookieHardShadows.shaderKeywords = new string[] {"POINT_COOKIE", "SHADOWS_CUBE"};
		PointCookieSoftShadows.shaderKeywords = new string[] {"POINT_COOKIE", "SHADOWS_CUBE", "SHADOWS_SOFT"};
	}

	public static void RefreshAll ()
	{
		foreach (var light in PipLightSystem.instance.m_Lights) {
			light.UpdateNextFrame = true;
			light.UpdateLOD ();
			light.UpdateIfNeeded ();
		}
	}

	void ReconstructLightBuffers (Camera camera, bool toCull)
	{
		CommandBuffer cameraBuffer = null;
		buffers.TryGetValue (camera, out cameraBuffer);
		if (cameraBuffer != null) {
			cameraBuffer.Clear ();
		} else {
			cameraBuffer = new CommandBuffer ();
			cameraBuffer.name = "Deferred custom lights";
			camera.AddCommandBuffer (CameraEvent.BeforeImageEffectsOpaque, cameraBuffer);
			buffers.Add (camera, cameraBuffer);
		}
		var system = PipLightSystem.instance;
		Bounds bounds = new Bounds ();
		Plane[] frustrumPlanes = null;

		if (toCull) {
			frustrumPlanes = GeometryUtility.CalculateFrustumPlanes (camera);
		}
		foreach (var light in system.m_Lights) {
			bool toRenderThisLight = true;
			light.UpdateLOD ();
			if (toCull) {
				bounds.center = light.transform.position;
				bounds.extents = Vector3.one * light.range;
				toRenderThisLight = GeometryUtility.TestPlanesAABB (frustrumPlanes, bounds);
			}
			if (toRenderThisLight) {
				light.UpdateIfNeeded ();
				cameraBuffer.DrawMesh (
					lightSphereMesh, 
					Matrix4x4.TRS (light.transform.position, Quaternion.identity, Vector3.one * light.range * 2f), 
					GetMaterial (light),
					0, 
					0,
					light.GetMaterialPropertyBlock ());
			}
		}
	}

	public void LateUpdate ()
	{
		foreach (var light in PipLightSystem.instance.m_Lights) {
			light.renderedThisFrame = false;
		}
		if (GetComponent<Camera> () != null) {
			ReconstructLightBuffers (GetComponent<Camera> (), true);
		}
		#if UNITY_EDITOR
		SceneViewCameras = SceneView.GetAllSceneCameras ();
		for (int i = 0; i < SceneViewCameras.Length; i++) {
			ReconstructLightBuffers (SceneViewCameras [i], false);
		}
		#endif
	}
}
