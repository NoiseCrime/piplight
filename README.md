# README #

### How do I set this up? ###

Add a PipLightRenderer component to any camera you desire to have support for these lights. It'll add the command buffers to the camera itself.
Then add the PipLight component to any gameobject intended to cast light, and it should work.

### How could this be useful? ###

By default, point lights remake their shadow maps every frame. If you've got a lot of static geometry to light, it'll do the same job again and again. This costs significant frame time. So this code re-uses the same shadowmap for multiple frames until you instruct it to update.

### Wait, this has automatic level of detail? ###

Yes, it does. Standard point lights seem to have no level of detail, making their performance worse in the distance as they could be. This is probably due to the shadowmap re-using which improves the performance  a lot, see above.

### Why doesn't shadow bias work? ###

Because the code references the .cginc's from unity - where they screwed up shadow bias. It's hard-coded to 0.03 in there.

### Why does this perform so much worse than Unity's point lights when updating every frame? ###

There are probably multiple reasons. Unity's point lights seem to use the same shadow map for each point light when possible, and there are probably some overpowered internal functions I can't access.
Re-using the shadowmap for multiple point lights isn't even possible at this moment, as there aren't CommandBuffer.DrawCamera / CommandBuffer.DrawCameraCubemap functions.

### Are there any bugs? ###

Yes. For some reason I can't fathom, the lights sometimes stop rendering after quitting a game preview.

### Can I use this on mobile? ###

Sure. It should automatically switch to the mobile quality BRDF. You should probably comment out the 2 defines at the top of the light shader (PIP_RANGE_DISCARD, PIP_SHADOW_DISCARD ?), as they're optimizing with discards.

### Why are you using a MaterialPropertyBlock instead of the commandbuffer for the properties? ###

Unity rewrites a lot of the builtin variables somewhere between the commandbuffer commands and applying the materialpropertyblock. =(